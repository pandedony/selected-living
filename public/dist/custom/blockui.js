function blockUI() {
  $.blockUI({
    css: {
      backgroundColor: 'transparent',
      border: 'none'
    },
    message: '<div class="spinner-border" role="status">',
    overlayCSS: {
      backgroundColor: '#FFFFFF',
      opacity: 0.7,
      cursor: 'wait'
    }
  });
};

function blockUiSuccess(msg) {
  $.blockUI({
    message: `<div class="text-dark p-3"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-check2-circle" viewBox="0 0 16 16">
    <path d="M2.5 8a5.5 5.5 0 0 1 8.25-4.764.5.5 0 0 0 .5-.866A6.5 6.5 0 1 0 14.5 8a.5.5 0 0 0-1 0 5.5 5.5 0 1 1-11 0z"/>
    <path d="M15.354 3.354a.5.5 0 0 0-.708-.708L8 9.293 5.354 6.646a.5.5 0 1 0-.708.708l3 3a.5.5 0 0 0 .708 0l7-7z"/>
  </svg> ${msg}</div>`, 
    timeout: ($.unblockUI, 2000),
    showOverlay: false,
    css: { 
      width: '20%', 
      top: '60px', 
      left : '40%',
      right : 'auto',
      margin: 'auto',
      border: 'none', 
      backgroundColor: '#8BEDB4', 
      '-webkit-border-radius': '10px', 
      '-moz-border-radius': '10px', 
      opacity: .9, 
      color: '#000' 
    }  
  })
};

function blockUiError(error) {
  $.blockUI({
    message: `<div class="text-white p-3"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-circle" viewBox="0 0 16 16">
    <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
    <path d="M7.002 11a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 4.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 4.995z"/>
  </svg> ${error}</div>`, 
    timeout: ($.unblockUI, 2000),
    showOverlay: false,
    css: { 
      width: '20%', 
      top: '60px', 
      left : '40%',
      right : 'auto',
      margin: 'auto',
      border: 'none', 
      backgroundColor: '#F62710', 
      '-webkit-border-radius': '10px', 
      '-moz-border-radius': '10px', 
      opacity: .9, 
      color: '#000' 
    }  
  })
};