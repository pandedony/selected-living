ClassicEditor.create(document.querySelector("#description"), {
    removePlugins: ["Heading"],
    toolbar: ["bold", "italic", "bulletedList", "numberedList"],
}).catch((error) => {
    console.error(error);
});
$("#productImgUpload").change(function () {
    let reader = new FileReader();
    reader.onload = (e) => {
        $("#productImgUploadPreview").attr("src", e.target.result);
    };
    reader.readAsDataURL(this.files[0]);
});

$(document).on("click", ".saveProduct", function () {
    blockUI();
});

var productDatatable;
$(function () {
    productDatatable = $(".product-datatables").DataTable({
        processing: true,
        rowId: "id",
        serverSide: true,
        ajax: "/dashboard/product/getjson",
        columns: [
            {
                data: "DT_RowIndex",
                name: "DT_RowIndex",
                orderable: false,
                searchable: false,
            },
            { data: "imgProduct", name: "product_img" },
            { data: "title", name: "title", className: "longtext" },
            { data: "category.name", name: "category.name", searchable: true },
            { data: "idrPrice", name: "idrPrice" },
            { data: "productStatus", name: "productStatus" },
            { data: "productNew", name: "productNew" },
            { data: "description", name: "description", className: "longtext" },
            {
                data: "action",
                name: "action",
                orderable: false,
                searchable: false,
            },
        ],
        select: true,
    });
});

var contactDatatables;
$(function () {
    productDatatable = $(".message-datatables").DataTable({
        processing: true,
        rowId: "id",
        serverSide: true,
        ajax: "/dashboard/message/getjson",
        columns: [
            {
                data: "DT_RowIndex",
                name: "DT_RowIndex",
                orderable: false,
                searchable: false,
            },
            { data: "name", name: "name" },
            { data: "email", name: "email"},
            { data: "subject", name: "subject"},
            { data: "description", name: "description"},
            { data: "contactStatus", name: "contactStatus"},
            {
                data: "action",
                name: "action",
                orderable: false,
                searchable: false,
            },
        ],
        select: true,
    });
});

var collectionDatatables;
$(function () {
    collectionDatatables = $(".collection-datatables").DataTable({
        processing: true,
        rowId: "id",
        serverSide: true,
        ajax: "/dashboard/product/".tab,
        columns: [
            {
                data: "DT_RowIndex",
                name: "DT_RowIndex",
                orderable: false,
                searchable: false,
            },
            { data: "imgProduct", name: "product_img" },
            { data: "title", name: "title", className: "longtext" },
            {
                data: "category.name",
                name: "category.name",
                searchable: true,
            },
            { data: "idrPrice", name: "idrPrice" },
            { data: "productStatus", name: "productStatus" },
            { data: "productNew", name: "productNew" },
            {
                data: "description",
                name: "description",
                className: "longtext",
            },
            {
                data: "action",
                name: "action",
                orderable: false,
                searchable: false,
            },
        ],
        select: true,
    });
});
