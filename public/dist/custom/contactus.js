$('#contactusform').submit(function(e){
  e.preventDefault();
  var formdata = new FormData(this); //$(this).serialize();
  
  $.ajax({
    type:'POST',
    url:"/contact/store",
    data: formdata,
    processData: false,
    contentType: false,
    beforeSend : function() {
      blockUI();
    }, 
    success:function(data){
      $.unblockUI();
      if (data.success == true) {
        document.getElementById("contactusform").reset();
        // $("#addcollectionmodal").modal("hide")
        blockUiSuccess(data.message)
      }
      else{
        blockUiError(data.message)
      }
    }
  });
});