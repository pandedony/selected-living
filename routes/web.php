<?php

use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\CollectionController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/asd/asd', function () {
    return view('handler.404');
});

Route::group(['middleware' => ['auth']], function() {
    Route::controller(AdminController::class)->group(function () {
        Route::get('/dashboard/message', 'index')->name('dashboard.message');
        Route::get('/dashboard/message/getjson', 'contactGetjson');
        Route::get('/dashboard/message/{id}', 'show')->name('dashboard.message.show');
    });

    Route::controller(ProductController::class)->group(function () {
        Route::get('/dashboard', 'index')->name('dashboard');
        Route::get('/dashboard/product/show/{id}', 'show')->name('product.show');
        Route::get('/dashboard/product/create', 'create')->name('product.create');
        Route::post('/dashboard/product/store', 'store')->name('product.store');
        Route::get('/dashboard/product/edit/{id}', 'edit')->name('product.edit');
        Route::post('/dashboard/product/update/{id}', 'update')->name('product.update');
        Route::delete('/dashboard/product/delete/{id}', 'destroy')->name('product.destroy');
        Route::get('/dashboard/product/getjson', 'ProductJson')->name('product.getjson');
    });

    Route::controller(CollectionController::class)->group(function () {
        Route::get('/dashboard/product/{tab}', 'CollectionsJson');
    });
});



Route::controller(AuthController::class)->group(function () {
    Route::get('/login', 'login')->name('login');
    Route::post('/login', 'authenticate')->name('auth.login');
    Route::post('/logout', 'logout')->name('auth.logout')->middleware('auth');
});


// Route::controller(CollectionController::class)->prefix('/dashboard')->group(function () {
//     Route::get('/{tab}', 'index');
// });
Route::controller(HomeController::class)->group(function () {
    Route::get('/', 'index')->name('home');
    Route::get('/contact', 'contact')->name('home.contact');
    Route::post('/contact/store', 'cotactusStore')->name('home.contact.store');
    Route::get('/product/show/{id}', 'show')->name('home.show');
    Route::get('/collections/{tab}', 'collections');
    Route::get('/{tab}', 'homecollection');
});