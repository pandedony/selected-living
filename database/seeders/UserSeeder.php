<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->updateOrInsert(
            ['email' => 'test1@test.com'],
            [
            'name' => 'Admin',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('admin1234'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            ]
          );
  
          DB::table('users')->updateOrInsert(
            ['email' => 'test2@test.com'],
            [
            'name' => 'Admin',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('selected1234'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            ]
          );
  
          DB::table('users')->updateOrInsert(
            ['email' => 'test3@test.com'],
            [
            'name' => 'Admin',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('living1234'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            ]
          );
    }
}
