<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert(
            [
            'name' => 'Pottery',
            'route' => '/pottery',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            ]
          );
        DB::table('categories')->insert(
            [
            'name' => 'Furniture',
            'route' => '/furniture',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            ]
          );
        DB::table('categories')->insert(
            [
            'name' => 'Basket',
            'route' => '/basket',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            ]
          );
        DB::table('categories')->insert(
            [
            'name' => 'Lamp',
            'route' => '/lamp',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            ]
          );
        DB::table('categories')->insert(
            [
            'name' => 'Deco',
            'route' => '/deco',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            ]
          );
    }
}
