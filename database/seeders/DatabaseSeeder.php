<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Category;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        if (!User::exists()) {
            $this->call([
                UserSeeder::class,
            ]);
    
        }

        if (!Category::exists()) {
            $this->call([
                CategorySeeder::class,
            ]);
    
        }

    }
}
