<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use Yajra\DataTables\Facades\DataTables;

class ProductController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    return view('dashboard.index');
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $categories = Category::all();
    return view('dashboard.product.create', compact('categories'))->render();
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $adminId = Auth::id();
    $request->validate([
      'title' => 'required|string',
      'category' => 'exists:App\Models\Category,id',
      'status' => 'required|integer',
      'new_item' => 'required|integer',
      'description' => 'required|string',
      'product_img' => 'image|mimes:jpeg,png,jpg,gif|max:10000|required',
    ]);

    $file = $request->file('product_img');
    $fileName = time() . "_" . $file->getClientOriginalName();

    Product::create([
      'title' => $request->title,
      'category_id' => $request->category,
      'status' => $request->status,
      'new_item' => $request->new_item,
      'description' => $request->description,
      'product_img' => $fileName,
      'createdby' => $adminId,
    ]);


    $img = Image::make($file);
    if (Image::make($file)->width() > 720) {
      $img->resize(720, null, function ($constraint) {
        $constraint->aspectRatio();
      });
    }
    $uploadDirectory = 'dist/images/upload/product/';
    $img->save($uploadDirectory . $fileName, 80);

    //   $file->move($uploadDirectory,$fileName);

    return redirect('/dashboard')->with('message', 'Product successfully added');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $decryptid = Crypt::decryptString($id);
    $product = Product::findOrFail($decryptid);

    return view('dashboard.product.show', compact('product', 'id'))->render();
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $decryptid = Crypt::decryptString($id);
    $product = Product::findOrFail($decryptid);
    $categories = Category::where('id', '!=', $product->category_id)->get();

    return view('dashboard.product.edit', compact('product', 'categories', 'id'))->render();
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $adminId = Auth::id();
    $decryptid = Crypt::decryptString($id);
    $product = Product::findOrFail($decryptid);
    $oldImage = $product->product_img;
    $uploadDirectory = 'dist/images/upload/product/';

    if ($request->product_img) {
      $request->validate([
        'product_img' => 'image|mimes:jpeg,png,jpg,gif|max:10000|required',
      ]);
      $file = $request->file('product_img');
      $fileName = time() . "_" . $file->getClientOriginalName();
    } else if (!$request->product_img)
      $fileName = $product->product_img; {
    }

    $request->validate([
      'title' => 'required|string',
      'category' => 'exists:App\Models\Category,id',
      'status' => 'required|integer',
      'new_item' => 'required|integer',
      'description' => 'required|string',
    ]);

    Product::where('id', $decryptid)->update([
      'title' => $request->title,
      'category_id' => $request->category,
      'status' => $request->status,
      'new_item' => $request->new_item,
      'description' => $request->description,
      'product_img' => $fileName,
      'updated_at' => Carbon::now(),
      'createdby' => $adminId,
    ]);

    if($fileName != $product->product_img)
    {
      $img = Image::make($file);
      if (Image::make($file)->width() > 720) {
        $img->resize(720, null, function ($constraint) {
          $constraint->aspectRatio();
        });
      }
      $img->save($uploadDirectory . $fileName, 80);
      File::delete($uploadDirectory . $oldImage);
    }

    return redirect('/dashboard/product/show/'.$id)->with('message', 'Product successfully updated');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $decryptid = Crypt::decryptString($id);
    $product = Product::findOrFail($decryptid);
    $deleteDirectory = 'dist/images/upload/product/'. $product->product_img;
    File::delete($deleteDirectory);
    $product->delete();

    return redirect('/dashboard')->with('message', 'Item deleted successfully');
  }

  public function ProductJson(Request $request)
  {
    if ($request->ajax()) { 
    $model = Product::query()->orderBy('id', 'DESC')->with('category');

    return DataTables::eloquent($model)
      ->addIndexColumn()
      ->addColumn('imgProduct', function ($product) {
        $url = asset("/dist/images/upload/product/$product->product_img");
        return '<img class="square-image-100" src="' . $url . '">';
      })
      ->addColumn('productStatus', function ($product) {
        if ($product->status == Product::STATUS_ACTIVE) {
          return 'Active';
        } elseif ($product->status == Product::STATUS_INACTIVE) {
          return 'Inactive';
        }
      })
      ->addColumn('productNew', function ($product) {
        if ($product->new_item == Product::STATUS_NEW_ITEM) {
          return 'New Item';
        } elseif ($product->new_item == Product::STATUS_OLD_ITEM) {
          return 'Old Item';
        }
      })
      ->addColumn('action', function ($product) {
        $encryptid = Crypt::encryptString($product->id);
        return view('dashboard.product.action', compact('encryptid'))->render();
      })
      ->escapeColumns([])
      ->make(true);
    }
    else{
      return view('handler.404');
    }
  }
}
