<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Yajra\DataTables\Facades\DataTables;

class AdminController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    return view('dashboard.message.contact');
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function ProductCreate()
  {
    return view('dashboard.product.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    //
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $decryptid = Crypt::decryptString($id);
    $contact = Contact::findOrFail($decryptid);
    Contact::where('id', $decryptid)->update([
      'status' => '1',
    ]);

    return view('dashboard.message.show', compact('contact', 'id'))->render();
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    //
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    //
  }

  public function messageIndex()
  {
  }

  public function contactGetjson(Request $request)
  {
    if ($request->ajax()) {
      $model = Contact::query()->orderBy('id', 'DESC');

      return DataTables::eloquent($model)
        ->addIndexColumn()
        ->addColumn('contactStatus', function ($contact) {
          if ($contact->status == Contact::STATUS_READ) {
            return 'Readed';
          } elseif ($contact->status == Contact::STATUS_UN_READ) {
            return 'unread';
          }
        })
        ->addColumn('action', function ($contact) {
          $encryptid = Crypt::encryptString($contact->id);
          return view('dashboard.message.contacaction', compact('encryptid'))->render();
        })
        ->escapeColumns([])
        ->make(true);
    } else {
      return view('handler.404');
    }
  }
}
