<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Yajra\DataTables\Facades\DataTables;

class CollectionController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    //
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    //
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    //
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    //
  }

  public function CollectionsJson(Request $request, $tab)
  {
    if ($request->ajax()) {
      if ($tab == 'pottery') {
        $model = Product::query()
          ->where('category_id', '1')
          ->orderBy('id', 'DESC')->with('category');
      } else if ($tab == 'furniture') {
        $model = Product::query()
          ->where('category_id', '2')
          ->orderBy('id', 'DESC')->with('category');
      } else if ($tab == 'lamp') {
        $model = Product::query()
          ->where('category_id', '4')
          ->orderBy('id', 'DESC')->with('category');
      } else if ($tab == 'deco') {
        $model = Product::query()
          ->where('category_id', '5')
          ->orderBy('id', 'DESC')->with('category');
      } else if ($tab == 'basket') {
        $model = Product::query()
          ->where('category_id', '3')
          ->orderBy('id', 'DESC')->with('category');
      }
      return DataTables::eloquent($model)
        ->addIndexColumn()
        ->addColumn('imgProduct', function ($product) {
          $url = asset("/dist/images/upload/product/$product->product_img");
          return '<img class="square-image-100" src="' . $url . '">';
        })
        ->addColumn('idrPrice', function ($product) {
          return "Rp. " . number_format($product->price, 0, ',', '.');
        })
        ->addColumn('productStatus', function ($product) {
          if ($product->status == Product::STATUS_ACTIVE) {
            return 'Active';
          } elseif ($product->status == Product::STATUS_INACTIVE) {
            return 'Inactive';
          }
        })
        ->addColumn('productNew', function ($product) {
          if ($product->new_item == Product::STATUS_NEW_ITEM) {
            return 'New Item';
          } elseif ($product->new_item == Product::STATUS_OLD_ITEM) {
            return 'Old Item';
          }
        })
        ->addColumn('action', function ($product) {
          $encryptid = Crypt::encryptString($product->id);
          return view('dashboard.product.action', compact('encryptid'))->render();
        })
        ->escapeColumns([])
        ->make(true);
    } else {

      return view('dashboard.colindex', [
        'tab'          => $tab,
      ]);
    }
  }
}
