<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Throwable;

class HomeController extends Controller
{
  public function index()
  {
    $products = Product::where('new_item', '1')
    ->where('status', '1')
    ->paginate(12);
    $status = 'new arrival';
    return view('home.index', compact('products', 'status'))->render();
  }

  public function contact()
  {
    return view('home.contact');
  }

  public function homecollection($tab)
  {
    if ($tab == 'pottery') {
      $status = 'pottery';
      $products = Product::where('category_id', '1')
      ->where('status', '1')
      ->paginate(12);
    } else if ($tab == 'furniture') {
      $status = 'furniture';
      $products = Product::where('category_id', '2')
      ->where('status', '1')
      ->paginate(12);
    } else if ($tab == 'lamp') {
      $status = 'lamp';
      $products = Product::where('category_id', '4')
      ->where('status', '1')
      ->paginate(12);
    } else if ($tab == 'deco') {
      $status = 'deco';
      $products = Product::where('category_id', '5')
      ->where('status', '1')
      ->paginate(12);
    } else if ($tab == 'basket') {
      $status = 'basket';
      $products = Product::where('category_id', '3')
      ->where('status', '1')
      ->paginate(12);
    } else {
      return view('handler.404');
    }

    return view('home.index', compact('products', 'status'))->render();
  }

  public function collections($tab)
  {
    $newproducts = Product::where('new_item', '1')
      ->where('status', '1')
      ->orderBy('updated_at', 'DESC')->take('8')->get();
    if ($tab == 'pottery') {
      $collections = 'pottery';
      $products = Product::where('category_id', '1')
      ->where('status', '1')
      ->paginate(12);
    } else if ($tab == 'furniture') {
      $collections = 'furniture';
      $products = Product::where('category_id', '2')
      ->where('status', '1')
      ->paginate(12);
    } else if ($tab == 'lamp') {
      $collections = 'lamp';
      $products = Product::where('category_id', '4')
      ->where('status', '1')
      ->paginate(12);
    } else if ($tab == 'deco') {
      $collections = 'deco';
      $products = Product::where('category_id', '5')
      ->where('status', '1')
      ->paginate(12);
    } else if ($tab == 'basket') {
      $collections = 'basket';
      $products = Product::where('category_id', '3')
      ->where('status', '1')
      ->paginate(12);
    } else if ($tab == 'all') {
      $collections = 'all collections';
      $products = Product::where('status', '1')
      ->paginate(12);
    } else {
      return view('handler.404');
    }
    return view('home.collection', compact('products', 'collections', 'newproducts'))->render();
  }

  public function show($id)
  {
    try {
      $decryptid = Crypt::decryptString($id);
      $product = Product::findOrFail($decryptid);
      $collections = Product::where('id', '!=', $product->id)
        ->where('status', '1')
        ->where('category_id', $product->category_id)->paginate(12);
    } catch (Throwable $e) {
      return view('handler.404');
    }
    return view('home.show', compact('product', 'collections'))->render();
  }

  public function cotactusStore(Request $request)
  {
    try {
      $request->validate([
        'name' => 'required|string',
        'email' => 'required|email:dns',
        'subject' => 'required|string',
        'description' => 'required|string',
      ]);

      Contact::create([
        'name' => $request->name,
        'email' => $request->email,
        'subject' => $request->subject,
        'description' => $request->description,
      ]);

      return response()->json([
        'success' => true,
        'message' => "Thank you for contact us.",
      ], 200);
    } catch (\Exception $e) {
      return response()->json(['message' => $e->getMessage(),]);
    }
  }
}
