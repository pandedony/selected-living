<?php

namespace App\Providers;

use App\Models\Category;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppMenuProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $phone = [
            'number' => '6282144310564',
            'name' => '+62 821 4431 0564'
        ];
        if (Schema::hasTable('categories')) {
            $menus = Category::all();
            if(!$menus)
            {
                return "please run migration";
            }
    
            View::share('global_sidebar_menus', $menus);
            View::share('phonenumber', $phone);
        }

    }
}
