<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'price',
        'category_id',
        'status',
        'new_item',
        'description',
        'product_img',
        'createdby',
      ];



    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    const STATUS_NEW_ITEM = 1;
    const STATUS_OLD_ITEM = 0;

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }
}
