<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
  use HasFactory;

  protected $fillable = [
    'name',
    'email',
    'subject',
    'description',
  ];

  const STATUS_READ = 1;
  const STATUS_UN_READ= 0;
}
