### Project Setup

1. Clone project using SSH or HTTPS
2. Open terminal / CMD and direct to this project directory
3. Copy `.env.example` and rename it to `.env` open the file and adjust the environment variables
4. Run `composer install` to install all dependencies
5. Run `php artisan key:generate` to generate APP_KEY
6. Run `php artisan migrate` to create migration
7. Run `php artisan db:seed` to add seeder
