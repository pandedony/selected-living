@extends('layouts.adminlayouts.main')

@section('title', 'Login - ')

@section('content')

  <div class="vertical-align-wrap">
    <div class="vertical-align-middle">
      <div class="auth-box col-lg-4">
        <div class="content">
          <div class="header">
            <div class="logo text-center">
              <h3>SELECTED LIVING</h3>
            </div>
            <p class="lead">Login to your account</p>
            @if (session()->has('loginError'))
              <div class="alert alert-danger" role="alert">
                {{ session('loginError') }}
              </div>
            @endif

          </div>
          <form class="form-auth-small" method="POST" action="{{ route('auth.login') }}">
            @csrf
            <div class="form-group">
              <label for="signin-email" class="control-label sr-only">Email</label>
              <input type="text" name="email" class="@error('email') is-invalid @enderror form-control" id="signin-email" placeholder="Email">
              @error('email')
                <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
              @enderror
            </div>
            <div class="form-group">
              <label for="signin-password" class="@error('password') is-invalid @enderror control-label sr-only">Password</label>
              <input type="password" name="password" class="form-control" id="signin-password" placeholder="Password">
              @error('password')
                <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
              @enderror
            </div>
            <button type="submit" class="btn btn-primary btn-lg btn-block">LOGIN</button>
            <div class="bottom">
              <span class="helper-text"><a href="{{ route('home') }}"><i class="fa fa-arrow-left mr-2"></i> Back to home</a></span>
            </div>
          </form>
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>

@endsection
