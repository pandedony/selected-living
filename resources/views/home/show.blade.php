@extends('layouts.homelayouts.main')

@section('title', 'Detail - ')

@section('content')

  @include('layouts.homelayouts.navbar')


  <!-- contact section start -->
  <section class="py-5">
    <div class="container px-4 px-lg-5 my-5">
      <div class="row gx-4 gx-lg-5 align-items-center">

        <div class="col-md-6">
          @if ($product->new_item == 1)
            <div class="ribbon ribbon-top-left bg-success"><span>new</span></div>
          @endif
          <img data-toggle="modal" data-target="#exampleModalCenter" class="img-ratio-11 card-img-top mb-5 mb-md-0" src="{{ asset('dist/images/upload/product/' . $product->product_img) }}" alt="..." />
        </div>
        <div class="col-md-6">

          <div class="small mb-1">{{ $product->category->name }}</div>
          <h1 class="display-5 fw-bolder">{{ $product->title }}</h1>
          <p class="lead">{!! $product->description !!}</p>
          <div class="d-flex">
            <button class="btn btn-outline-dark flex-shrink-0" type="button">
              <i class="bi-cart-fill me-1"></i>
              <a href="https://api.whatsapp.com/send?phone={{ $phonenumber['number'] }}" onclick="return confirm('Open whatsapp?')">
                Shop Now
              </a>
            </button>
          </div>
        </div>
      </div>
    </div>
  </section>


  <section class="py-5 bg-green-01">
    <div class="container px-4 px-lg-5 mt-5">
      <h2 class="fw-bolder mb-4">Related products</h2>
      <div class="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4">
        @foreach ($collections as $collection)
          <div class="col-lg-3 col-6 my-2">
            <div class="card shadow">
              @if ($collection->new_item == 1)
                <div class="ribbon ribbon-top-left bg-success"><span>new</span></div>
              @endif
              <a href="{{ route('home.show', Crypt::encryptString($collection->id)) }}">
                <img class="card-img-top square-image" src="{{ asset('dist/images/upload/product/' . $collection->product_img) }}" alt="Card image cap">
                <div class="card-body">
                  <h5 class="card-title nowraptitle">{{ $collection->title }} </h5>
                  <h6 class="card-subtitle text-muted">{{ $collection->category->name }}</h6>
                </div>
              </a>
              <a class="btn btn-info col-12" href="https://api.whatsapp.com/send?phone={{ $phonenumber['number'] }}" onclick="return confirm('Open whatsapp?')">Shop Now</a>
            </div>
          </div>
        @endforeach
        <div class="col-12 text-center my-3">
            {{ $collections->withQueryString()->links('pagination::bootstrap-5') }}
         </div>
      </div>
    </div>
  </section>


  <!-- Modal -->
  <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <img class="card-img-top mb-5 mb-md-0" src="{{ asset('dist/images/upload/product/' . $product->product_img) }}" alt="..." />
        </div>
      </div>
    </div>
  </div>
  </div>

  @include('layouts.homelayouts.footer')

@endsection
