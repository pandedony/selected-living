@extends('layouts.homelayouts.main')

@section('title', 'Collections - ')

@section('content')

  @include('layouts.homelayouts.navbar')

  <!-- New Arrival section start -->
  <div class="services_section layout_padding layout_padding_banner mt-5">
    <div class="container">

      <div class="border-bottom border-secondary row">
        <h1 class="services_taital">{{ $collections }}</h1>
      </div>
    </div>
    <div class="container">
      <div class="services_section2 layout_padding">
        <div class="row">

          @foreach ($products as $product)
            <div class="col-lg-3 col-6 my-2">
              <div class="card shadow">
                @if ($product->new_item == 1)
                  <div class="ribbon ribbon-top-left bg-success"><span>new</span></div>
                @endif
                <a href="{{ route('home.show', Crypt::encryptString($product->id)) }}">
                  <img class="card-img-top square-image" src="{{ asset('dist/images/upload/product/' . $product->product_img) }}" alt="Card image cap">
                  <div class="card-body">
                    <h5 class="card-title nowraptitle">{{ $product->title }} </h5>
                    <h6 class="card-subtitle text-muted">{{ $product->category->name }}</h6>
                  </div>
                </a>
                <a class="btn btn-info col-12" href="https://api.whatsapp.com/send?phone={{ $phonenumber['number'] }}" onclick="return confirm('Open whatsapp?')">Shop Now</a>
              </div>
            </div>
          @endforeach
        <div class="col-12 text-center my-3">
          {{ $products->withQueryString()->links('pagination::bootstrap-5') }}
        </div>


        </div>
      </div>
    </div>
  </div>
  <!-- New Arrival section end -->

  <!-- services section start -->
  <div class="services_section layout_padding">
    <div class="container">
      <div class="border-bottom border-secondary row">
        <h1 class="services_taital">other collections</h1>
      </div>
    </div>
    <div class="container">
      <div class="services_section2 layout_padding ">
        <div class="row">

          @foreach ($newproducts as $new)
            <div class="col-lg-3 col-6 my-2">
              <div class="card shadow">
                @if ($new->new_item == 1)
                  <div class="ribbon ribbon-top-left bg-success"><span>new</span></div>
                @endif
                <a href="{{ route('home.show', Crypt::encryptString($new->id)) }}">
                  <img class="card-img-top square-image" src="{{ asset('dist/images/upload/product/' . $new->product_img) }}" alt="Card image cap">
                  <div class="card-body">
                    <h5 class="card-title nowraptitle">{{ $new->title }} </h5>
                    <h6 class="card-subtitle text-muted">{{ $new->category->name }}</h6>
                  </div>
                </a>
                <a class="btn btn-info col-12" href="https://api.whatsapp.com/send?phone={{ $phonenumber['number'] }}" onclick="return confirm('Open whatsapp?')">Shop Now</a>
              </div>
            </div>
          @endforeach

        </div>
      </div>
    </div>
  </div>
  <!-- services section end -->

  <!-- contact section start -->


  @include('layouts.homelayouts.contact')
  @include('layouts.homelayouts.footer')

@endsection
