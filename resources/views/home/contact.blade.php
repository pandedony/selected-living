@extends('layouts.homelayouts.main')

@section('title', 'Contact - ')

@section('content')

  @include('layouts.homelayouts.navbar')


  <!-- contact section start -->
  <div class="contact_section layout_padding layout_padding_banner mt-5">
    <div class="container">
      <div class="border-bottom border-secondary row">
        <h1 class="services_taital">contact us</h1>
      </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-md-6">

          <div class="container">
            <div class="row">
              <div class="col-md-6">
                <div class="p-2 bg-green-01 rounded my-1">
                  <i class="bi bi-geo-alt-fill fa-3x text-green"></i>
                  <h2 class="text-green">Location</h2>
                  <p> <a class="text-info" href="https://goo.gl/maps/dak1c3GfKjWLWgiZ7">JL. RAYA KEROBOKAN NO.115, BR. TAMAN 80361, KEROBOKAN, BADUNG, BALI. </a></p>
                </div>
              </div>
              <div class="col-md-6">
                <div class="p-2 bg-green-01 rounded my-1">
                  <i class="bi bi-telephone-fill fa-3x text-green"></i>
                  <h2 class="text-green">Phone</h2>
                  <p><a class="text-info" href="https://api.whatsapp.com/send?phone={{ $phonenumber['number'] }}" onclick="return confirm('Open whatsapp?')">{{ $phonenumber['name'] }}</a></p>
                </div>
              </div>
              <div class="col-md-6">
                <div class="p-2 bg-green-01 rounded my-1">
                  <i class="bi bi-envelope-fill fa-3x text-green"></i>
                  <h2 class="text-green">Mail</h2>
                  <p><a class="text-info" href="mailto: selected.shop@gmail.com"> selected.shop@gmail.com</a></p>
                </div>
              </div>
              <div class="col-md-6">
                <div class="p-2 bg-green-01 rounded my-1">
                  <i class="bi bi-clock-fill fa-3x text-green"></i>
                  <h2 class="text-green">Open Hour</h2>
                  <p>09:00 am to 06.00 pm</p>
                </div>
              </div>
            </div>
          </div>

        </div>

        <div class="col-md-6 my-1">
          <div class="mx-1 p-2 bg-green-01 rounded">
            <form id="contactusform">
              @csrf
              <div class="form-row">
                <div class="form-group col-md-6">
                  <label for="inputName">Name</label>
                  <input name="name" type="text" class="form-control" id="inputName" placeholder="Name">
                </div>
                <div class="form-group col-md-6">
                  <label for="inputEmail">Email</label>
                  <input name="email" type="email" class="form-control" id="inputEmail" placeholder="Email">
                </div>
              </div>
              <div class="form-group">
                <label for="inputSubject">Subject</label>
                <input name="subject" type="text" class="form-control" id="inputSubject" placeholder="Subject">
              </div>
              <div class="form-group">
                <label for="inputDescription">Message</label>
                <textarea name="description" class="form-control" id="inputDescription" rows="3" placeholder="Message"></textarea>
              </div>
              <div class="text-center">

                <button type="submit" class="btn btn-primary">Send Message</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

  @include('layouts.homelayouts.footer')

@endsection
