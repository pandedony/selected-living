@extends('layouts.homelayouts.main')

@section('title', 'Home - ')

@section('content')

  @include('layouts.homelayouts.navbar')


  <div class="banner_section layout_padding_banner">
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner ">
        <div class="carousel-caption">
          <h1 class="furniture_text">WELCOME </h1>
          <h1 class="furniture_text" style="font-size: 3vw!important;">TO SELECTED LIVING</h1>
          <h1 class="furniture_text" style="font-size: 2.3vw;">HOME ACCESSORIES AND FURNITURE</p>
            <h1 class="furniture_text" style="font-size: 1.3vw;">Jl. Raya Kerobokan No.115, Br. Taman 80361, Kerobokan, Badung, Bali.</h1>
            <a href="https://api.whatsapp.com/send?phone={{ $phonenumber['number'] }}" onclick="return confirm('Open whatsapp?')">
              <h1 class="furniture_text text-green" style="font-size: 1.5vw;">{{ $phonenumber['name'] }}</h1>
            </a>
            <p>
              <a class="btn btn-info" href="https://api.whatsapp.com/send?phone={{ $phonenumber['number'] }}" onclick="return confirm('Open whatsapp?')">Contact Us</a>
            </p>
        </div>
        <div class="carousel-item active">
          <img class="d-block w-100" src="{{ asset('dist/images/banner/banner3.jpg') }}" alt="First slide">
        </div>
        <div class="carousel-item">
          <img class="d-block w-100" src="{{ asset('dist/images/banner/banner4.jpg') }}" alt="Second slide">
        </div>
        <div class="carousel-item">
          <img class="d-block w-100" src="{{ asset('dist/images/banner/banner5.jpg') }}" alt="Third slide">
        </div>
        <div class="carousel-item">
          <img class="d-block w-100" src="{{ asset('dist/images/banner/banner6.jpg') }}" alt="Third slide">
        </div>
      </div>
      <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
  </div>

  <!-- New Arrival section start -->
  <div class="services_section layout_padding">
    <div class="container">

      <div class="row">
        <h1 class="services_taital">{{ $status }}</h1>
      </div>
      <div class="row border-top border-bottom border-secondary">
        <div class="col-12 text-center">
          <div class="featured__controls my-2">
            <ul>
              <li class="{{ request()->is('/') ? 'active' : '' }}">
                <a href="{{ url('/') }}" class="">New Item</a>
              </li>
              @foreach ($global_sidebar_menus as $menu)
                <li class="{{ request()->is($menu->route) ? 'active' : '' }}">
                  <a href="{{ url($menu->route) }}" class="">{{ $menu->name }}</a>
                </li>
              @endforeach
            </ul>

          </div>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="services_section2 layout_padding">
        <div class="row">

          @foreach ($products as $product)
            <div class="col-lg-3 col-6 my-2">
              <div class="card shadow">
                @if ($product->new_item == 1)
                  <div class="ribbon ribbon-top-left bg-success"><span>new</span></div>
                @endif
                <a href="{{ route('home.show', Crypt::encryptString($product->id)) }}">
                  <img class="card-img-top square-image" src="{{ asset('dist/images/upload/product/' . $product->product_img) }}" alt="Card image cap">
                  <div class="card-body">
                    <h5 class="card-title nowraptitle">{{ $product->title }} </h5>
                    <h6 class="card-subtitle text-muted">{{ $product->category->name }}</h6>
                  </div>
                </a>
                <a class="btn btn-info col-12" href="https://api.whatsapp.com/send?phone={{ $phonenumber['number'] }}" onclick="return confirm('Open whatsapp?')">Shop Now</a>
              </div>
            </div>
          @endforeach


          <div class="col-12 text-center my-3">
            {{ $products->withQueryString()->links('pagination::bootstrap-5') }}
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- New Arrival section end -->

  <!-- services section start -->
  <div class="services_section layout_padding">
    <div class="container">
      <div class="border-bottom border-secondary row">
        <h1 class="services_taital">our collections</h1>
      </div>
    </div>
    <div class="container">
      <div class="services_section2 layout_padding ">
        <div class="row">

          <div class="col-lg-3 col-6 my-2">
            <a href="{{ url('collections/pottery') }}">
              <div class="card shadow">
                <img class="card-img-top square-image" src="{{ asset('dist/images/collections/pottery.jpg') }}" alt="Card image cap">
                <div class="card-body">
                  <h5 class="card-title text-center text-white bg-info rounded my-1">POTTERY </h5>
                </div>
              </div>
            </a>
          </div>

          <div class="col-lg-3 col-6 my-2">
            <a href="{{ url('collections/basket') }}">
              <div class="card shadow">
                <img class="card-img-top square-image" src="{{ asset('dist/images/collections/basket.jpg') }}" alt="Card image cap">
                <div class="card-body ">
                  <h5 class="card-title text-center text-white bg-info rounded my-1">BASKET</h5>
                </div>
              </div>
            </a>
          </div>

          <div class="col-lg-3 col-6 my-2">
            <a href="{{ url('collections/deco') }}">
              <div class="card shadow">
                <img class="card-img-top square-image" src="{{ asset('dist/images/collections/deco.jpg') }}" alt="Card image cap">
                <div class="card-body">
                  <h5 class="card-title text-center text-white bg-info rounded my-1">DECO</h5>
                </div>
              </div>
            </a>
          </div>

          <div class="col-lg-3 col-6 my-2">
            <a href="{{ url('collections/furniture') }}">
              <div class="card shadow">
                <img class="card-img-top square-image" src="{{ asset('dist/images/collections/furniture.jpg') }}" alt="Card image cap">
                <div class="card-body">
                  <h5 class="card-title text-center text-white bg-info rounded my-1">FURNITURE</h5>
                </div>
              </div>
            </a>
          </div>

          <div class="col-lg-3 col-6 my-2">
            <a href="{{ url('collections/lamp') }}">
              <div class="card shadow">
                <img class="card-img-top square-image" src="{{ asset('dist/images/collections/lamp.jpg') }}" alt="Card image cap">
                <div class="card-body">
                  <h5 class="card-title text-center text-white bg-info rounded my-1"> LAMP</h5>
                </div>
              </div>
            </a>
          </div>

        </div>
      </div>
    </div>
  </div>
  <!-- services section end -->

  <!-- about section start -->
  <div class="about_section layout_padding">
    <div class="container">
      <div class="row text-dark">
        <div class="col-md-6">
          <h1 class="about_text">About Us</h1>
          <p class="lorem_text">Furniture refers to movable objects intended to support various human activities such as seating (e.g., stools, chairs, and sofas), eating (tables), storing items,
            eating and/or working with an item, and sleeping (e.g., beds and hammocks). Furniture is also used to hold objects at a convenient height for work (as horizontal surfaces above the ground,
            such as tables and desks), or to store things (e.g., cupboards, shelves, and drawers). Furniture can be a product of design and can be considered a form of decorative art. In addition to
            furniture's functional role, it can serve a symbolic or religious purpose. It can be made from a vast multitude of materials, including metal, plastic, and wood. Furniture can be made using
            a variety of woodworking joints which often reflects the local culture.</p>
        </div>
        <div class="col-md-6">
          <div class="image_1"><img src="{{ asset('dist/images/img-1.png') }}"></div>
        </div>
      </div>
    </div>
  </div>
  <!-- about section end -->

  <!-- contact section start -->


  @include('layouts.homelayouts.contact')
  @include('layouts.homelayouts.footer')

@endsection
