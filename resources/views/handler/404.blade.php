@extends('layouts.homelayouts.main')

@section('title', '404 - ')

@section('content')


  <div class="wrapper d-flex align-items-center" style="width:100%; height:100vh;">
    <div class="w-100 text-center">
      <h1 class="" style="font-size:100px !important;">404 | Page not found</h1>
    </div>
  </div>


@endsection
