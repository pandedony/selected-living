<div id="sidebar-nav" class="sidebar">
  <div class="sidebar-scroll">
    <nav>
      <ul class="nav w-100">
        <li class="w-100">
          <a href="{{ route('dashboard') }}" class="{{ request()->is('dashboard') ? 'active' : '' }}{{ request()->is('dashboard/product/create') ? 'active' : '' }}"><i class="lnr lnr-home"></i>
            <span>Dashboard</span></a>
        </li>
        <li class="w-100">
          <a href="#subPages" data-toggle="collapse" class="collapsed"><i class="lnr lnr-file-empty"></i> <span>Collections</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
          <div id="subPages" class="active">
            <ul class="nav">
              @foreach ($global_sidebar_menus as $menu)
                <li class="w-100 {{ request()->is('pottery') ? 'active' : '' }}">
                  <a href="{{ url('dashboard/product', $menu->route) }}" class=" {{ request()->is('dashboard/product/' . $menu->route) ? 'active' : '' }}">{{ $menu->name }}</a>
                </li>
              @endforeach
            </ul>
          </div>
        </li>
        <li class="w-100">
          <a href="{{ route('dashboard.message') }}" class="{{ request()->is('dashboard/message') ? 'active' : '' }}"><i class="lnr lnr-code"></i> <span>Message</span></a>
        </li>
      </ul>
    </nav>
  </div>
</div>
