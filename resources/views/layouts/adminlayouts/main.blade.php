<!DOCTYPE html>
<html lang="en">
  <head>
    <title>@yield('title')Selected Living</title>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <!-- VENDOR CSS -->

    <link rel="stylesheet" type="text/css" href="{{asset('dist/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('dist/vendor/font-awesome/css/font-awesome.min.css')}}" />
    <link rel="stylesheet" href="{{asset('dist/vendor/linearicons/style.css')}}" />
    <link rel="stylesheet" href="{{asset('dist/vendor/datatables/dataTables.bootstrap4.css')}}" />

    <link rel="stylesheet" href="{{asset('dist/css/klorofil.css')}}" />
    <link rel="stylesheet" href="{{asset('dist/css/klorofil-custom.css')}}" />

    <!-- GOOGLE FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet" />
    <!-- ICONS -->
    <link rel="icon" href="{{asset('dist/images/logo/logo.png')}}">
  </head>

  <body>
    <!-- WRAPPER -->
    <div id="wrapper">
      <!-- NAVBAR -->

      <!-- END NAVBAR -->
      <!-- LEFT SIDEBAR -->
      
      <!-- END LEFT SIDEBAR -->
      <!-- MAIN -->
      @yield('content')
      <!-- END MAIN -->

    </div>
    <!-- END WRAPPER -->
    <!-- Javascript -->
    <script src="{{asset('dist/js/jquery363.js')}}"></script>
    <script src="{{asset('dist/js/bootstrap.min.js')}}"></script>

    <script src="{{asset('dist/vendor/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('dist/vendor/datatables/dataTables.bootstrap4.js')}}"></script>
    <script src="{{asset('dist/vendor/datatables/dataTables.select.min.js')}}"></script>

    <script src="{{asset('dist/vendor/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('dist/vendor/ckeditor/ckeditor.js')}}"></script>
    <script src="{{asset('dist/vendor/block-ui/blockui.js')}}"></script>
    <script src="{{asset('dist/custom/blockui.js')}}"></script>
    <script src="{{asset('dist/js/klorofil-common.js')}}"></script>
    @yield('script')
  </body>
</html>
