<nav class="navbar navbar-expand-lg navbar-light bg-ligh navbar-default fixed-top">
  <div class="brand">
    <a class="navbar-brand" href="{{route('dashboard')}}">SELECTED LIVING</a>
  </div>
  <div class="container-fluid collapse navbar-collapse">
    <div class="navbar-btn">
      <button type="button" class="btn-toggle-fullwidth">
        <i class="lnr lnr-arrow-left-circle"></i>
      </button>
    </div>
    <div id="navbar-menu ">
      <div class="btn-group">
        <a href="#" class="dropdown-toggle pr-2" data-toggle="dropdown">
          <span>Administrator</span>
        </a>
        <div class="dropdown-menu dropdown-menu-right">
          <form action="{{route('auth.logout')}}" method="POST">
            @csrf
            <button class="dropdown-item" type="submit">Logout</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</nav>
