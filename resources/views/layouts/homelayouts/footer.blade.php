<!-- copyright section start -->
<div class="copyright_section bg-green-05">
  <div class="container">
    <div class="social_icon">
      <ul>
        <li><a href="https://www.instagram.com/selectedliving2011"><img src="{{ asset('dist/images/instagram-icon.png') }}"></a></li>
      </ul>
    </div>
    <p class="copyright_text">Selected Living &copy; 2023 by <a href="https://id.linkedin.com/in/pande-dony-a62547230" target="_blank">Pande Dony</a></p>
  </div>
</div>
