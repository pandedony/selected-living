<!DOCTYPE html>
<html lang="en">
   <head>
      <!-- basic -->
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- mobile metas -->
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="viewport" content="initial-scale=1, maximum-scale=1">
      <!-- site metas -->
      <title>@yield('title')Selected Living</title>

      <meta name="keywords" content="Furniture, Pottery, Basket, Lamp, Deco">
      <meta name="description" content="SELECTED LIVING HOME ACCESSORIES AND FURNITURE">
      <meta name="author" content="Selected Living">
      <!-- bootstrap css -->

      <link rel="stylesheet" type="text/css" href="{{asset('dist/css/bootstrap.min.css')}}">
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.3/font/bootstrap-icons.css">
      <!-- style css -->
      <link rel="stylesheet" type="text/css" href="{{asset('dist/css/style.css')}}">
      <link rel="stylesheet" type="text/css" href="{{asset('dist/css/custom.css')}}">
      <!-- Responsive-->
      <link rel="stylesheet" href="{{asset('dist/css/responsive.css')}}">
      <!-- fevicon -->
      <link rel="icon" href="{{asset('dist/images/logo/logo.png')}}">
      <!-- Scrollbar Custom CSS -->
      <link rel="stylesheet" href="{{asset('dist/css/jquery.mCustomScrollbar.min.css')}}">
      <!-- Tweaks for older IEs-->
      <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
      <!-- owl stylesheets --> 

      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">
   </head>
   <body>


      @yield('content')

      <script src="{{asset('dist/js/jquery363.js')}}"></script>

      <script src="{{asset('dist/js/bootstrap.min.js')}}"></script>
      <script src="{{asset('dist/js/popper.min.js')}}"></script>
      <script src="{{asset('dist/vendor/block-ui/blockui.js')}}"></script>
      <script src="{{asset('dist/custom/blockui.js')}}"></script>
      <script src="{{asset('dist/custom/contactus.js')}}"></script>

      <script src="{{asset('dist/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>


      <script src="https:cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>    
   </body>
</html>