<div class="header_section  ">
  <nav class="navbar navbar-expand-lg navbar-light bg-white fixed-top shadow">
    <a class="navbar-brand logo" href="{{ route('home') }}"><img src="{{ asset('dist/images/logo/logo.png') }}" alt="">
      SELECTED LIVING</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarText">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="{{ url('/') }}">Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Collections
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="{{ url('collections/all') }}">Show All</a>
            @foreach ($global_sidebar_menus as $menu)
              <a class="dropdown-item" href="{{ url('collections', $menu->route) }}">{{ $menu->name }}</a>
            @endforeach
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{ url('contact') }}">Contact</a>
        </li>
      </ul>
      <span class="navbar-text">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            
            <a class="nav-link text-green" href="https://api.whatsapp.com/send?phone={{$phonenumber['number']}}" onclick="return confirm('Open whatsapp?')">Call Us : {{$phonenumber['name']}}</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('login') }}">Login</a>
          </li>
        </ul>
      </span>
    </div>
  </nav>
</div>
