@extends('layouts.adminlayouts.main')

@section('title', 'dashboard - ')

@section('content')

  @include('layouts.adminlayouts.topbar')
  @include('layouts.adminlayouts.leftbar')
  <!-- MAIN CONTENT -->
  <div class="main">
    <div class="main-content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12 bg-white rounded px-3 py-1">
            <div class="row">
              <div class="col-12">
                <h3 class="card-title my-2">Message</h3>
              </div>
              @if (session()->has('message'))
                <div class="alert alert-success col-12" role="alert">
                  {{ session('message') }}
                </div>
              @endif
              <div class="col-12">
                <div class="table-responsive">
                  <table class="table table-hover message-datatables table-striped" id="datatable" style="width:100%">
                    <thead class="">
                      <tr>
                        <th class="w-1">No.</th>
                        <th scope="col">Name</th>
                        <th scope="col">Email</th>
                        <th scope="col">Subject</th>
                        <th scope="col">Description</th>
                        <th scope="col">Status</th>
                        <th scope="col">Action</th>
                      </tr>
                    </thead>

                    <tbody>
                      <tr>
                        <td colspan="7" class="text-center">No data available</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- END MAIN CONTENT -->


@endsection
@section('script')
  <script src="{{ asset('dist/custom/product.js') }}"></script>
@endsection
