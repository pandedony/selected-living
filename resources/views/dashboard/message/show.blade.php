@extends('layouts.adminlayouts.main')

@section('title', 'dashboard - ')

@section('content')

  @include('layouts.adminlayouts.topbar')
  @include('layouts.adminlayouts.leftbar')
  <!-- MAIN CONTENT -->
  <div class="main">
    <div class="main-content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-9 rounded bg-white px-3 py-1">
            <div class="row">

              <div class="col-12">
                <h3 class="card-title my-2">Show Message</h3>
              </div>
              <div class="col-12">
                <div class="container">
                  <form>
                    <div class="row">
                      <div class="col-lg-8">
                        <div class="form-group">
                          <label for="inputTitle">Name</label>
                          <div class="form-control">{{$contact->name}}</div>
                        </div>
                        <div class="form-group">
                          <label for="inputTitle">Email</label>
                          <div class="form-control">{{$contact->email}}</div>
                        </div>
                        <div class="form-group">
                          <label for="inputTitle">Subject</label>
                          <div class="form-control">{{$contact->subject}}</div>
                        </div>
                        <div class="form-group">
                          <label for="inputTitle">Description</label>
                          <div class="form-control">{{$contact->description}}</div>
                        </div>

                      </div>

                      <div class="col-lg-12">
                        <div class="form-group text-right">
                          <a href="{{ route('dashboard.message') }}" class="btn btn-secondary">Back</a>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- END MAIN CONTENT -->


@endsection

@section('script')
  <script src="{{ asset('dist/custom/product.js') }}"></script>
@endsection
