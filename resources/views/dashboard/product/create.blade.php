@extends('layouts.adminlayouts.main')

@section('title', 'dashboard - ')

@section('content')

  @include('layouts.adminlayouts.topbar')
  @include('layouts.adminlayouts.leftbar')
  <!-- MAIN CONTENT -->
  <div class="main">
    <div class="main-content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-9 rounded bg-white px-3 py-1">
            <div class="row">

              <div class="col-12">
                <h3 class="card-title my-2">Add Product</h3>
              </div>
              <div class="col-12">
                <div class="container">
                  <form action="{{ route('product.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                      <div class="col-lg-8">
                        <div class="form-group">
                          <label for="inputTitle">Title</label>
                          <input type="text" name="title" class="form-control" id="inputTitle" placeholder="Title" value="{{ old('title') }}">
                          @error('title')
                            <small class="form-text text-danger">{{ $message }}</small>
                          @enderror
                        </div>
                        <div class="form-row">
                          <div class="form-group col-md-6">
                            <label for="category">Category</label>
                            <select id="category" class="form-control" name="category">
                              <option selected value="">Choose...</option>
                              @foreach ($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                              @endforeach
                            </select>
                            @error('category')
                              <small class="form-text text-danger">{{ $message }}</small>
                            @enderror
                          </div>
                          <div class="form-group col-md-6">
                            <label for="status">Status</label>
                            <select id="status" class="form-control" name="status">
                              <option selected value="">Choose...</option>
                              <option value="1">Active</option>
                              <option value="0">In Active</option>
                            </select>
                            @error('status')
                              <small class="form-text text-danger">{{ $message }}</small>
                            @enderror
                          </div>
                          <div class="form-group col-md-6">
                            <label for="newItem">New Item</label>
                            <select id="newItem" class="form-control" name="new_item">
                              <option selected value="">Choose...</option>
                              <option value="1">Yes</option>
                              <option value="0">No</option>
                            </select>
                            @error('new_item')
                              <small class="form-text text-danger">{{ $message }}</small>
                            @enderror
                          </div>

                        </div>
                        <div class="form-group">
                          <label for="inputAddress2">Description</label>
                          <textarea class="form-control" id="description" name="description" rows="10">{{ old('description') }}</textarea>
                          @error('description')
                            <small class="form-text text-danger">{{ $message }}</small>
                          @enderror
                        </div>
                      </div>
                      <div class=" col-lg-4 justify-content-center">
                        <div class="form-group ">
                          <div class="col-md-12 mb-2">
                            <img id="productImgUploadPreview" src="{{ asset('dist/images/preview/preview.jpg') }}" alt="preview image" style="max-width: 100%;">
                          </div>
                          <input class="form-control" type="file" id="productImgUpload" name="product_img">
                          @error('product_img')
                            <small class="form-text text-danger">{{ $message }}</small>
                          @enderror
                        </div>
                      </div>
                      <div class="col-lg-12">
                        <div class="form-group text-right">
                          <a href="{{route('dashboard')}}" class="btn btn-secondary">Cancel</a>
                          <button type="submit" class="btn btn-primary saveProduct">Create</button>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- END MAIN CONTENT -->


@endsection

@section('script')
  <script src="{{ asset('dist/custom/product.js') }}"></script>
@endsection
