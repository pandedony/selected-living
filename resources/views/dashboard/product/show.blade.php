@extends('layouts.adminlayouts.main')

@section('title', 'dashboard - ')

@section('content')

  @include('layouts.adminlayouts.topbar')
  @include('layouts.adminlayouts.leftbar')
  <!-- MAIN CONTENT -->
  <div class="main">
    <div class="main-content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-9 rounded bg-white px-3 py-1">
            <div class="row">

              <div class="col-12">
                <h3 class="card-title my-2">Show Product</h3>
              </div>
              @if (session()->has('message'))
                <div class="alert alert-success col-12" role="alert">
                  {{ session('message') }}
                </div>
              @endif
              <div class="col-12">
                <div class="container">
                  <form>
                    <div class="row">
                      <div class="col-lg-8">
                        <div class="form-group">
                          <label for="inputTitle">Title</label>
                          <input type="text" name="title" class="form-control" id="inputTitle" placeholder="Title" value="{{ $product->title }}" disabled>
                        </div>
                        <div class="form-row">
                          <div class="form-group col-md-6">
                            <label for="category">Category</label>
                            <select id="category" class="form-control" name="category" disabled>
                              <option selected value="{{ $product->category_id }}">{{ $product->category->name }}</option>
                            </select>
                          </div>
                          <div class="form-group col-md-6">
                            <label for="status">Status</label>
                            <select id="status" class="form-control" name="status" disabled>
                              @if ($product->status == 0)
                                <option value="{{ $product->status }}">In Active</option>
                              @elseif($product->status == 1)
                                <option value="{{ $product->status }}">Active</option>
                              @endif
                            </select>
                          </div>
                          <div class="form-group col-md-6">
                            <label for="newItem">New Item</label>
                            <select id="newItem" class="form-control" name="new_item" disabled>
                              @if ($product->new_item == 0)
                                <option value="{{ $product->new_item }}">Old Item</option>
                              @elseif($product->new_item == 1)
                                <option value="{{ $product->new_item }}">New Item</option>
                              @endif
                            </select>
                          </div>

                        </div>
                        <div class="form-group">
                          <label for="inputAddress2">Description</label>
                          <div class="form-control">
                            {!! $product->description !!}
                          </div>
                        </div>
                      </div>
                      <div class=" col-lg-4 justify-content-center">
                        <div class="form-group ">
                          <div class="col-md-12 mb-2">
                            <img id="productImgUploadPreview" src="{{ asset('dist/images/upload/product/' . $product->product_img) }}" alt="preview image" style="max-width: 100%;">
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-12">
                        <div class="form-group text-right">
                          <a href="{{ route('dashboard') }}" class="btn btn-secondary">Back</a>
                          <a href="{{ route('product.edit', $id) }}" class="btn btn-primary">Edit</a>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- END MAIN CONTENT -->


@endsection

@section('script')
  <script src="{{ asset('dist/custom/product.js') }}"></script>
@endsection
