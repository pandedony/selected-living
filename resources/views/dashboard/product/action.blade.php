
<form action="{{route('product.destroy', $encryptid)}}" method="post">
  @csrf
  @method('delete')
  <a href="{{route('product.edit', $encryptid)}}" class="btn btn-sm btn-primary rounded" title="Edit Product">
    <i class="fa fa-pencil" aria-hidden="true"></i>
  </a>
  <a href="{{route('product.show', $encryptid)}}" class="btn btn-sm btn-info rounded" title="View product on dashboard">
    <i class="fa fa-eye" aria-hidden="true"></i>
  </a>
  <a href="{{route('home.show', $encryptid)}}" class="btn btn-sm btn-secondary rounded" title="View product on homepage" target="_blank">
    <i class="fa fa-paper-plane" aria-hidden="true"></i>
  </a>
  <button class="btn btn-sm btn-danger rounded" type="submit" onclick="return confirm('Are you sure want to delete this item?')" title="Delete product">
    <i class="fa fa-trash" aria-hidden="true"></i>
  </button>
</form>
