@extends('layouts.adminlayouts.main')

@section('title', 'dashboard - ')

@section('content')

  @include('layouts.adminlayouts.topbar')
  @include('layouts.adminlayouts.leftbar')
  <!-- MAIN CONTENT -->
  <div class="main">
    <div class="main-content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12 bg-white rounded px-3 py-1">
            <div class="row">
              <div class="col-6">
                <h3 class="card-title my-2">Collections</h3>
              </div>
              <div class="col-6 text-right">
                {{-- <button class="asd" id="asd">asd</button> --}}
                <a href="{{ route('product.create') }}"><button class="btn btn-primary  rounded collectionlink my-1" data-toggle="modal" data-target="#addcollectionmodal">Add Collection</button></a>
              </div>
              @if (session()->has('message'))
                <div class="alert alert-success col-12" role="alert">
                  {{ session('message') }}
                </div>
              @endif
              <div class="col-12">
                <div class="table-responsive">
                  <table class="table table-hover collection-datatables table-striped" id="datatable" style="width:100%">
                    <thead class="">
                      <tr>
                        <th class="w-1">No.</th>
                        <th scope="col">Picture</th>
                        <th scope="col">Title</th>
                        <th scope="col">Category</th>
                        <th scope="col">Price</th>
                        <th scope="col">Status</th>
                        <th scope="col">Item</th>
                        <th scope="col">Description</th>
                        <th scope="col">Action</th>
                      </tr>
                    </thead>

                    <tbody>
                      <tr>
                        <td colspan="9" class="text-center">No data available</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- END MAIN CONTENT -->


@endsection
@section('script')
  <script type="text/javascript">
    var table = null;
    var tab = "{{ $tab }}";
  </script>
  <script src="{{ asset('dist/custom/product.js') }}"></script>
@endsection
